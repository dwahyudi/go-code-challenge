package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type (
	Coordinate struct {
		X, Y float64
	}

	CoordOwner map[Direction]string
	Direction  string
)

const (
	NW Direction = "NORTHWEST"
	NE Direction = "NORTHEAST"
	SW Direction = "SOUTHWEST"
	SE Direction = "SOUTHEAST"
	BR Direction = "BORDER"
)

var (
	mrAbdiCaseOwners = CoordOwner{
		NW: "Johan",
		NE: "Budi",
		SW: "Didi",
		SE: "Tono",
		BR: "Perbatasan",
	}
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	for done := false; !done; {
		done = execTerminal(input)
	}
}

func execTerminal(input *bufio.Scanner) bool {
	owners := make([]string, 0)

	var err error

	input.Scan()

	// get N
	testCasesInput := input.Text()
	testCasesNum, err := strconv.Atoi(testCasesInput)
	if err != nil || testCasesNum < 0 {
		log.Print("must input number greater than 0")
	}

	if testCasesNum == 0 {
		return true
	}

	// get wedge coordinate
	input.Scan()
	wedgeCoordInput := input.Text()
	wedgeCoordText := strings.Split(wedgeCoordInput, " ")

	wedgeX, err := strconv.ParseFloat(wedgeCoordText[0], 64)
	if err != nil {
		log.Print("error wedge x coordinate")
	}

	wedgeY, err := strconv.ParseFloat(wedgeCoordText[1], 64)
	if err != nil {
		log.Print("error wedge y coordinate")
	}

	wedge := Coordinate{float64(wedgeX), float64(wedgeY)}

	// instantiate the xy border service
	xyBorder := NewXYBorder(
		mrAbdiCaseOwners,
		wedge,
	)

	// for N cases collect one by one coord for ownership checking
	for i := 0; i < testCasesNum; i++ {
		input.Scan()
		coordInput := input.Text()

		coordInputText := strings.Split(coordInput, " ")

		coordX, err := strconv.ParseFloat(coordInputText[0], 64)
		if err != nil {
			log.Print("error wedge x coordinate")
		}

		coordY, err := strconv.ParseFloat(coordInputText[1], 64)
		if err != nil {
			log.Print("error wedge y coordinate")
		}

		coord := Coordinate{float64(coordX), float64(coordY)}

		owner := xyBorder.GetOwner(coord)
		owners = append(owners, owner)
	}

	for _, eachOwner := range owners {
		fmt.Println(eachOwner)
	}

	return false
}

// TODO: move this to a new file/package for better reuse.
type (
	XYBorderCalculator interface {
		GetOwner(coord Coordinate) string
	}

	xyBorderCalculator struct {
		owners CoordOwner
		wedge  Coordinate
	}
)

func NewXYBorder(coordOwners CoordOwner, wedge Coordinate) XYBorderCalculator {
	return &xyBorderCalculator{
		owners: coordOwners,
		wedge:  wedge,
	}
}

func (b *xyBorderCalculator) GetOwner(coord Coordinate) string {
	var direction Direction
	if coord.X < b.wedge.X && coord.Y > b.wedge.Y { // "NORTHWEST"
		direction = NW
	} else if coord.X > b.wedge.X && coord.Y > b.wedge.Y { // "NORTHEAST"
		direction = NE
	} else if coord.X < b.wedge.X && coord.Y < b.wedge.Y { // "SOUTHWEST"
		direction = SW
	} else if coord.X > b.wedge.X && coord.Y < b.wedge.Y { // "SOUTHEAST"
		direction = SE
	} else { // "BORDER"
		direction = BR
	}

	return b.owners[direction]
}
