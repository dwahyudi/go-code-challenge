# Problem

Mr. Abdi is a large farm owner with 4 children. Currently Pak Johan wants to share his land with his four children, namely Johan, Budi, Didi and Tono. To make the division easier, Mr. Abdi will divide his land into 4 parts by placing a stake somewhere in his field. Each child will get one of the quadrants of the field:

    Johan will get the North West quadrant
    Budi will get the North East quadrant
    Didi will get the Southwest quadrant
    Tono will get the Southeast quadrant

After being satisfied with this method, Mr. Abdi remembered that he had assets in several plots of land. He wants to know in whose hands these assets will end up. For this reason, he asks you to create a program to find out this.

Input Format

The input has several test cases. Each test case will start with a number N, namely the number of asset plots that Mr. Abdi wants to know. In the next line, the numbers X and Y will be given, namely the coordinates (x,y) of the plot where Mr. Abdi will place the peg. For the next N rows, 2 numbers A and B will be given, namely the coordinates (x,y) of the asset plot that Mr. Abdi wants to know. Input will end when N is 0. For the X-axis coordinate, which is the horizontal axis, -1000000 is the leftmost end, while 1000000 is the rightmost end. For the Y axis coordinate, which is the vertical axis, -1000000 is the lowest end, while 1000000 is the upper end.

Constraints

```
0 <= N <= 100

-1.000.000 <= X,Y <= 1.000.000

-1.000.000 <= A,B <= 1.000.000
```

Output Format

For each asset plot coordinate input, print the name of the asset owner on the screen ( Johan / Budi / Didi / Tono ). If the given asset tile coordinates are on a border, print the Border on the screen.

Sample Input 0

```
5
5 5
10 10
0 0
3 8
8 3
5 2
4
-99999 999
-1000 -1000
748 8292
-2123 0
1872 123
0
```

Sample Output 0

```
Budi
Didi
Johan
Tono
Perbatasan
Tono
Budi
Tono
Tono
```
