package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	input := bufio.NewScanner(os.Stdin)

	// get N & K count
	input.Scan()
	nAndKCount := input.Text()
	nAndKCountText := strings.Split(nAndKCount, " ")

	n, err := strconv.Atoi(nAndKCountText[0])
	if err != nil {
		log.Println("invalid N")
	}

	k, err := strconv.Atoi(nAndKCountText[1])
	if err != nil {
		log.Println("invalid K")
	}

	// get N data
	distances := make([]float64, 0)

	input.Scan()
	nInput := input.Text()
	nInputText := strings.Split(nInput, " ")
	for _, eachNInputText := range nInputText {
		distance, err := strconv.ParseFloat(eachNInputText, 64)
		if err != nil {
			log.Println("invalid max distance", eachNInputText)
		}

		if distance < 1 {
			log.Println("non-positive max distance", eachNInputText)
		}

		distances = append(distances, distance)
	}

	if len(distances) != n {
		log.Println("max distances input count should be =", n)
	}

	// get K data
	targets := make([]float64, 0)
	input.Scan()
	kInput := input.Text()
	kInputText := strings.Split(kInput, " ")
	for _, eachKInputText := range kInputText {
		target, err := strconv.ParseFloat(eachKInputText, 64)
		if err != nil {
			log.Println("invalid target", eachKInputText)
		}

		if target < 1 {
			log.Println("non-positive target distance", eachKInputText)
		}

		targets = append(targets, target)
	}

	if len(targets) != k {
		log.Println("targets distances input count should be =", n)
	}

	missileRange := NewMissileRange(distances, targets)
	doesHitAllTargets := missileRange.DoesHitAllTargets()

	if doesHitAllTargets {
		fmt.Println("Ya")
	} else {
		fmt.Println("Tidak")
	}
}

// TODO: move this to a new file/package for better reuse.
type (
	MissileRange interface {
		DoesHitAllTargets() bool
	}

	missileRange struct {
		maxDistances []float64
		targets      []float64
	}
)

func NewMissileRange(maxDistances []float64, targets []float64) MissileRange {
	return &missileRange{
		maxDistances: maxDistances,
		targets:      targets,
	}
}

func (m *missileRange) DoesHitAllTargets() bool {
	var doesHitAllTargets bool

	missilesCount := len(m.maxDistances)
	targetsCount := len(m.targets)

	// return immediately false when available missiles count is less than targets count
	if targetsCount > missilesCount {
		return false
	}

	// sort max distances ascendingly
	sort.Float64s(m.maxDistances)

	// sort target distances ascendingly
	sort.Float64s(m.targets)

	// ignore missiles that can't even reach the closest targets
	closestTargetDistance := m.targets[0]
	eligibleMissiles := make([]float64, 0)
	for _, eachMissileDistance := range m.maxDistances {
		if eachMissileDistance >= closestTargetDistance {
			eligibleMissiles = append(eligibleMissiles, eachMissileDistance)
		}
	}

	// return immediately false when eligible missiles count is less than targets count
	if targetsCount > len(eligibleMissiles) {
		return false
	}

	// simulate the missile strike, when hit, remove the target and the missile
	for _, eachTarget := range m.targets {
		for _, eachEligibleMissile := range eligibleMissiles {
			if eachTarget <= eachEligibleMissile {
				m.targets = m.targets[1:]
				m.maxDistances = m.maxDistances[1:]
				break
			}
		}
	}

	if len(m.targets) == 0 {
		doesHitAllTargets = true
	}

	return doesHitAllTargets
}
