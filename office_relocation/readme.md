PT Kokom has N employees in all its offices. Each employee has a level of ability which is represented by an integer K<sub>i</sub>. (The higher K<sub>i</sub>, the higher the ability of employee i). No 2 employees have the same level of ability.

PT Kokom wants to relocate every employee to one of the M offices it owns. It is known that the i-th office has an employee capacity of S<sub>i</sub>s. Apart from that, it is also known that:

S<sub>1</sub> + S<sub>2</sub> + ... + S<sub>M</sub> = N

The requirements that need to be met in relocation are stated as follows:

There cannot be an employee in the i<sup>th</sup> office who has higher abilities than an employee in one of the jth offices for every j < 1.

Help PT Kokom to determine the relocation mapping for each employee who meets these requirements.

Input Format

The first line contains integers N and M which indicate the number of employees and the number of offices owned by PT Kokom. The second line contains N integers K<sub>i</sub> which indicate the ability level of employee i. The third line contains M integers S<sub>i</sub> which represent the employee capacity in the i<sup>th</sup> office.

Constraints

* 1 ≤ N ≤ 1000
* 1 ≤ M ≤ N
* 1 ≤ K<sub>i</sub> ≤ 1000
* K<sub>i</sub> ≠ K<sub>j</sub>, untuk setiap i < j
* 1 ≤ S<sub>i</sub> ≤ N
* S<sub>1</sub> + S<sub>2</sub> + ... + S<sub>M</sub> = N

Output Format

Output N rows containing an integer R<sub>i</sub>, where R<sub>i</sub> represents the office number where employee i works after relocation.

Sample Input 0

```
5 3
4 5 2 6 3
2 1 2
```

Sample Output 0

```
2
1
3
1
3
```
