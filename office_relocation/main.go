package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	input := bufio.NewScanner(os.Stdin)

	// get N & M count
	input.Scan()
	nAndMCount := input.Text()
	nAndMCountText := strings.Split(nAndMCount, " ")

	n, err := strconv.Atoi(nAndMCountText[0])
	if err != nil {
		log.Println("invalid N")
	}

	m, err := strconv.Atoi(nAndMCountText[1])
	if err != nil {
		log.Println("invalid m")
	}

	// get staff skills
	staffSkills := make([]staffSkill, 0)
	input.Scan()
	skill := input.Text()
	skillInputText := strings.Split(skill, " ")

	for i, eachSkill := range skillInputText {
		staffSkillNum, err := strconv.ParseFloat(eachSkill, 64)
		if err != nil {
			log.Println("invalid skill value: ", eachSkill)
		}

		staffSkills = append(staffSkills, staffSkill{Skill: staffSkillNum, Index: i})
	}

	if len(staffSkills) != n {
		log.Println("staffs skills amount should be = ", n)
	}

	// get office caps
	officeCaps := make([]officeCap, 0)
	input.Scan()
	officeCapInput := input.Text()
	officeCapText := strings.Split(officeCapInput, " ")

	for j, eachOfficeCap := range officeCapText {
		officeCapNum, err := strconv.Atoi(eachOfficeCap)
		if err != nil {
			log.Println("invalid office cap value: ", eachOfficeCap)
		}

		officeCaps = append(officeCaps, officeCap{OfficeCap: officeCapNum, Index: j})
	}

	if len(officeCaps) != m {
		log.Println("office capacities amount should be = ", m)
	}

	relocation := NewRelocation(staffSkills, officeCaps)

	assignments := relocation.RelocateByStaffsSkill()

	sort.Slice(assignments, func(i, j int) bool {
		return assignments[i].Index < assignments[j].Index
	})

	for _, eachAssignment := range assignments {
		fmt.Println(eachAssignment.OfficeIndex + 1)
	}
}

// TODO: move this to a new file/package for better reuse.
type (
	Relocation interface {
		RelocateByStaffsSkill() []staffAssignment
	}

	staffSkill struct {
		Skill float64
		Index int
	}

	officeCap struct {
		OfficeCap int
		Index     int
	}

	relocation struct {
		staffSkills []staffSkill
		officeCaps  []officeCap
	}

	staffAssignment struct {
		Index       int
		OfficeIndex int
	}
)

func NewRelocation(
	staffSkills []staffSkill,
	officeCaps []officeCap,
) Relocation {
	return &relocation{
		staffSkills: staffSkills, officeCaps: officeCaps,
	}
}

func (r *relocation) RelocateByStaffsSkill() []staffAssignment {
	// sort the staffs by skills
	sort.Slice(r.staffSkills, func(i, j int) bool {
		return r.staffSkills[i].Skill > r.staffSkills[j].Skill
	})

	currentStaffCursor := 0
	staffAssignments := make([]staffAssignment, 0)

	// go through the offices
	for _, eachOfficeCap := range r.officeCaps {

		// assign each capacity to staffs (already sorted by skill)
		for j := eachOfficeCap.OfficeCap; j > 0; j-- {

			staffAssignments = append(staffAssignments, staffAssignment{Index: r.staffSkills[currentStaffCursor].Index, OfficeIndex: eachOfficeCap.Index})
			currentStaffCursor += 1

			eachOfficeCap.OfficeCap -= 1 // and reduce the capacity for this office
		}

	}

	return staffAssignments
}
