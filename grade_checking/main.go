package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
)

var (
	mrAbdiCaseStudents = []string{"Budi", "Didi", "Johan", "Tono"}
)

func main() {
	input := bufio.NewScanner(os.Stdin)

	// get N
	input.Scan()
	cardsNumInput := input.Text()
	cardsNum, err := strconv.Atoi(cardsNumInput)
	if err != nil || cardsNum < 0 {
		log.Println("must input number greater than 0")
	}

	studentsCount := len(mrAbdiCaseStudents)
	answerKeys := make([]float64, 0)

	// get answer key
	input.Scan()
	answerKey := input.Text()
	answerKeyText := strings.Split(answerKey, " ")
	if len(answerKeyText) != cardsNum {
		log.Println("answer key cards must be exact number of", cardsNum)
	}

	for _, eachAnswerKeyText := range answerKeyText {
		answerKey, err := strconv.ParseFloat(eachAnswerKeyText, 64)
		if err != nil {
			log.Println("invalid answer key card", eachAnswerKeyText)
		}

		answerKeys = append(answerKeys, answerKey)
	}

	// get students' answers
	cards := make(map[string][]float64, 0)
	for i := 0; i < studentsCount; i++ {
		studentCards := make([]float64, 0)

		input.Scan()
		studentAnswer := input.Text()
		studentAnswer = strings.TrimSuffix(studentAnswer, " ")
		studentAnswerText := strings.Split(studentAnswer, " ")
		if len(studentAnswerText) != cardsNum {
			log.Println("answer cards must be exact number of ", cardsNum)
		}

		for _, eachStudentAnswerText := range studentAnswerText {
			studentAnswer, err := strconv.ParseFloat(eachStudentAnswerText, 64)
			if err != nil {
				log.Println("invalid student card ", studentAnswer)
			}

			studentCards = append(studentCards, studentAnswer)
		}

		cards[mrAbdiCaseStudents[i]] = studentCards
	}

	cardOrderGrading := NewCardOrderGrading(answerKeys, cards)
	studentName := cardOrderGrading.getMostCorrectAnswer()

	fmt.Println(studentName)
}

// TODO: move this to a new file/package for better reuse.
type (
	CardOrderGrading interface {
		getMostCorrectAnswer() string
	}

	cardOrderGrading struct {
		answerKeys []float64
		cards      map[string][]float64
	}

	gradeCalculation struct {
		studentName string
		grade       int
	}

	gradeCalculationsMutex struct {
		*sync.RWMutex
		gradeCalculations []gradeCalculation
	}
)

func NewCardOrderGrading(answerKeys []float64, cards map[string][]float64) CardOrderGrading {
	return &cardOrderGrading{
		answerKeys: answerKeys,
		cards:      cards,
	}
}

func (c *cardOrderGrading) getMostCorrectAnswer() string {
	// TODO: build async pipes instead of waiting groups
	var wg sync.WaitGroup
	gradeCalculationsResult := gradeCalculationsMutex{gradeCalculations: []gradeCalculation{}, RWMutex: &sync.RWMutex{}}

	// first sort answer key cards
	sort.Float64s(c.answerKeys)

	for studentName, cards := range c.cards {
		wg.Add(1)

		go func(studentName string, cards []float64) {
			grade, resultStudentName := countGrade(c.answerKeys, cards, studentName)

			gradeCalculationsResult.RWMutex.Lock()
			defer gradeCalculationsResult.RWMutex.Unlock()
			gradeCalculationsResult.gradeCalculations = append(gradeCalculationsResult.gradeCalculations, gradeCalculation{studentName: resultStudentName, grade: grade})

			wg.Done()
		}(studentName, cards)
	}

	wg.Wait()

	sort.Slice(gradeCalculationsResult.gradeCalculations, func(i, j int) bool {
		iGrade := gradeCalculationsResult.gradeCalculations[i].grade
		jGrade := gradeCalculationsResult.gradeCalculations[j].grade
		iName := gradeCalculationsResult.gradeCalculations[i].studentName
		jName := gradeCalculationsResult.gradeCalculations[j].studentName

		if iGrade > jGrade {
			return true
		} else if iGrade == jGrade && iName < jName {
			return true
		} else {
			return false
		}
	})

	return gradeCalculationsResult.gradeCalculations[0].studentName
}

func countGrade(answerKey []float64, studentAnswer []float64, studentName string) (int, string) {
	var grade int
	length := len(answerKey)

	for i := 0; i < length; i++ {
		if answerKey[i] == studentAnswer[i] {
			grade += 1
		}
	}

	return grade, studentName
}
