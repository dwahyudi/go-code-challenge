# Problem

Mr. Abdi is a father with 4 children, namely Johan, Budi, Didi, and Tono. One of Mr. Abdi's favorite games with his children is searching for treasure on his land. How to play is as follows:

- First, Mr. Abdi's four children will choose one plot as a place to start looking for treasures on the land.
- Then, Mr. Abdi will choose a plot on the land to hide the treasure.
- The first person to find the treasure is the winner.

Currently, Mr. Abdi's children are very smart, so they can estimate the location of hidden treasure, and always take the shortest distance to get to the treasure. Mr. Abdi really wants to simulate this game in a program, so that Mr. Abdi can find out who is the winner in a game. As a good friend of Mr. Abdi, you are asked to help create a program to carry out this task.

Input Format

Input starts with 2 numbers, namely X and Y, namely the length (number of columns) and width (number of rows) of Mr. Abdi's land. The next X lines will contain Y characters each which is a map of Mr Abdi's field.

![Example image](example.png "Example")

Symbol . Symbolizes an empty plot. The symbol # represents a barrier wall that cannot be passed by Mr. Abdi's children. The symbols J, B, D, T represent the initial location of each of Mr. Abdi's children, according to their initials. It is ensured that in each input there is one child who succeeds in getting the treasure. The + symbol represents a plot containing treasure.

Constraints

```
5 <= X,Y <= 10
```

Output Format

Output the name of Mr. Abdi's child who won the game. If 2 or more children win the game, choose the name of the child with the smallest lexicographical order.

Sample Input 0

```
5 5
#####
..J..
.TBD.
.....
+....
```

Sample Output 0

```
Tono
```
