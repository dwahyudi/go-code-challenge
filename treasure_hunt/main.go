package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type (
	Coordinate struct {
		X, Y int
	}

	PlayerCoord struct {
		Name string
		Coordinate
	}
)

var (
	mrAbdiCase = map[string]string{
		"J": "Johan", "B": "Budi", "T": "Tono", "D": "Didi",
	}
)

func main() {
	input := bufio.NewScanner(os.Stdin)

	// get X & Y count
	input.Scan()
	xandYCount := input.Text()
	xandYCountText := strings.Split(xandYCount, " ")

	x, err := strconv.Atoi(xandYCountText[0])
	if err != nil {
		log.Println("invalid X")
	}

	y, err := strconv.Atoi(xandYCountText[1])
	if err != nil {
		log.Println("invalid Y")
	}

	// input each Y of X
	mapsAndPositions := make([][]string, 0)
	playerCoordinates := make([]PlayerCoord, 0)
	var treasureCoordinate Coordinate

	for i := 0; i < x; i++ {
		eachY := make([]string, 0)

		input.Scan()
		eachYInput := input.Text()
		eachYInputText := strings.Split(eachYInput, "")

		if len(eachYInputText) != y {
			log.Println("invalid column (y) numbers")
		}

		for j, token := range eachYInputText {
			eachY = append(eachY, token)

			switch token {
			case "J", "B", "T", "D":
				playerCoordinates = append(playerCoordinates, PlayerCoord{Name: mrAbdiCase[token], Coordinate: Coordinate{X: i, Y: j}})
			case "+":
				treasureCoordinate = Coordinate{X: i, Y: j}
			}
		}

		mapsAndPositions = append(mapsAndPositions, eachY)
	}

	thg := NewTreasureHuntGame(mapsAndPositions, playerCoordinates, treasureCoordinate, x, y)
	shortestPath := thg.GetShortestPath()

	fmt.Println(shortestPath)
}

// TODO: move this to a new file/package for better reuse.
type (
	TreasureHuntGame interface {
		GetShortestPath() string
	}

	treasureHuntGame struct {
		MapsAndPositions   [][]string
		PlayersCoords      []PlayerCoord
		TreasureCoordinate Coordinate
		X, Y               int
	}

	playerDistance struct {
		name  string
		count int
	}

	playerDistancesList struct {
		playerDistances []playerDistance
	}

	visitedCache struct {
		visitedPlaces [][]bool
	}
)

func NewTreasureHuntGame(
	mapsAndPositions [][]string,
	playersCoords []PlayerCoord,
	treasureCoordinate Coordinate,
	x int, y int,
) TreasureHuntGame {
	return &treasureHuntGame{
		MapsAndPositions:   mapsAndPositions,
		PlayersCoords:      playersCoords,
		TreasureCoordinate: treasureCoordinate,
		X:                  x,
		Y:                  y,
	}
}

func (th *treasureHuntGame) GetShortestPath() string {
	playerDistancesList := playerDistancesList{playerDistances: make([]playerDistance, 0)}
	// cache for saving already visited place
	visitedPlaces := make([][]bool, 0)
	for i := 0; i < th.X; i++ {
		eachJ := make([]bool, 0)
		for j := 0; j < th.Y; j++ {
			eachJ = append(eachJ, false)
		}
		visitedPlaces = append(visitedPlaces, eachJ)
	}
	cache := visitedCache{visitedPlaces: visitedPlaces}

	doExecShortestPath(th.MapsAndPositions, th.TreasureCoordinate, th.X, th.Y, 0, &playerDistancesList, &cache)

	// after obtaining every players' distance, sort by min distance
	sort.Slice(playerDistancesList.playerDistances, func(i, j int) bool {
		iName := playerDistancesList.playerDistances[i].name
		jName := playerDistancesList.playerDistances[j].name
		iCount := playerDistancesList.playerDistances[i].count
		jCount := playerDistancesList.playerDistances[j].count

		if iCount < jCount {
			return true
		} else if iCount == jCount && iName < jName {
			return true
		} else {
			return false
		}
	})

	return playerDistancesList.playerDistances[0].name
}

func doExecShortestPath(
	mapsAndPositions [][]string, currentCoord Coordinate, x int, y int, distances int, playerDistances *playerDistancesList, visitedPlaces *visitedCache,
) {
	currentX := currentCoord.X
	currentY := currentCoord.Y

	if currentX < 0 || currentY < 0 || currentX > x-1 || currentY > y-1 { // out of bound
		return
	}

	foundToken := mapsAndPositions[currentX][currentY]

	if foundToken == "#" { // hitting wall
		return
	}

	if visitedPlaces.visitedPlaces[currentX][currentY] { // this position has been visited
		return
	}

	visitedPlaces.visitedPlaces[currentX][currentY] = true

	name, ok := mrAbdiCase[foundToken]
	if ok { // player found
		playerDistances.playerDistances = append(playerDistances.playerDistances, playerDistance{name: name, count: distances})
	}

	nextXTraverseUp := currentX - 1
	ntuCurrentDistance := distances + 1
	doExecShortestPath(mapsAndPositions, Coordinate{X: nextXTraverseUp, Y: currentY}, x, y, ntuCurrentDistance, playerDistances, visitedPlaces)

	nextYTraverseLeft := currentY - 1
	ntlCurrentDistance := distances + 1
	doExecShortestPath(mapsAndPositions, Coordinate{X: currentX, Y: nextYTraverseLeft}, x, y, ntlCurrentDistance, playerDistances, visitedPlaces)

	nextXTraverseDown := currentX + 1
	ntdCurrentDistance := distances + 1
	doExecShortestPath(mapsAndPositions, Coordinate{X: nextXTraverseDown, Y: currentY}, x, y, ntdCurrentDistance, playerDistances, visitedPlaces)

	nextYTraverseRight := currentY + 1
	ntrCurrentDistance := distances + 1
	doExecShortestPath(mapsAndPositions, Coordinate{X: currentX, Y: nextYTraverseRight}, x, y, ntrCurrentDistance, playerDistances, visitedPlaces)
}
